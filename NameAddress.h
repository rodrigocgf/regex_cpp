#ifndef NAMEADDRESS_H
#define NAMEADDRESS_H

#include <string>
#include <map>
#include <regex>
#include <sstream>
#include <iostream>

using namespace std;
using Parameters = std::map<std::string, std::string>;

class NameAddress
{
public:
    //typedef std::map<std::string> Headers;
    //typedef std::map<std::string, std::string> Parameters;
    
    NameAddress();
    NameAddress(const NameAddress &nameAddress);
    bool isValid() const;
    const std::string &getDisplayName() const;
    const std::string &getUserName() const;
    const std::string &getHost() const;
    unsigned short getPort();

    bool getParameter(const std::string &name, const std::string &value) const;
    bool getHeader(const std::string &name, const std::string &value) const;
    bool parse(const std::string &nameAddress);
    NameAddress &operator=(const NameAddress &nameAddress);

private:
    bool _isValid;
    //Headers _headers;
    Parameters _parameters;
    std::string _displayName;
    std::string _userName;
    std::string _host;
    unsigned short _port;
};

//
// Inlines
//

inline bool NameAddress::isValid() const 
{ 
    return _isValid; 
}

inline const std::string &NameAddress::getDisplayName() const
{
    return _displayName;
}

inline const std::string &NameAddress::getUserName() const
{
    return _userName;
}

inline const std::string &NameAddress::getHost() const
{
    return _host;
}

inline unsigned short NameAddress::getPort()
{
    return _port;
}

#endif /* NAMEADDRESS_H */