
CFLAGS=g++ -m64 -Wall -std=c++1y -pthread

GTEST_DIR=/usr/src/googletest/
GTEST_INCLUDE=/usr/src/googletest/googletest/include
GTEST_SRCS_=/usr/src/googletest/googletest/src

INC=-I$(GTEST_INCLUDE)

LIB=-lgtest -lgtest_main
    
all: SipApp

SipApp : Sip.o NameAddress.o
	$(CFLAGS) -o SipApp Sip.o NameAddress.o $(LIB) $(INC)

Sip.o : Sip.cpp
	$(CFLAGS) -g -c Sip.cpp $(LIB) $(INC)

NameAddress.o :  NameAddress.cpp
	$(CFLAGS) -g -c NameAddress.cpp $(LIB) $(INC)

gtest-all.o : $(GTEST_SRCS_)
	$(CFLAGS) -I$(GTEST_DIR) -c $(GTEST_DIR)/src/gtest-all.cc

clean:
	rm -rf *.o
