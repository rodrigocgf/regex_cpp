#include "NameAddress.h"

NameAddress::NameAddress::NameAddress()
{

}

NameAddress::NameAddress(const NameAddress &obj)
{
    this->_isValid = obj._isValid;
    //this->_headers = obj._headers;
    //this->_parameters = obj._parameters;
    this->_displayName = obj._displayName;
    this->_userName = obj._userName;
    this->_host = obj._host;
    this->_port = obj._port;
}

bool NameAddress::getParameter(const string &name, const string &value) const
{

}

bool NameAddress::getHeader(const string &name, const string &value) const
{

}

bool NameAddress::parse(const string &nameAddress)
{
    bool bFound = false;

    //sip:192.168.1.10
    stringstream ss;

    ss << "sip:";
    ss << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).";
    ss << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).";
    ss << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).";
    ss << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


    regex reg_SipAddress(ss.str());

    if (regex_match(nameAddress, regex(reg_SipAddress)))
    {
        bFound = true;
        cout << "pattern <sip:ip address> found" << endl;

        string sIpAddr = nameAddress.substr(nameAddress.find(":") + 1, nameAddress.length());
        this->_host = sIpAddr;
        cout << "ip address : " << this->_host << endl;
    }

    //sip:192.168.1.10:5060
    stringstream ss1;

    ss1 << "sip:";
    ss1 << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).";
    ss1 << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).";
    ss1 << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).";
    ss1 << "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):[1-9][0-9]?[0-9]?[0-9]?";

    regex reg_SipAddressPort(ss1.str());

    if (regex_match(nameAddress, regex(reg_SipAddressPort))) 
    {
        bFound = true;
        cout << "pattern <sip:ip address:port> found" << endl;

        string sIpAddrPort = nameAddress.substr(nameAddress.find_first_of(":") + 1, nameAddress.length());
        string sIpAddr = sIpAddrPort.substr(0, sIpAddrPort.find(":"));
        string sPort = sIpAddrPort.substr(sIpAddrPort.find(":") + 1, sIpAddrPort.length());
        this->_host = sIpAddr;
        cout << "ip address : " << this->_host << endl;
        this->_port = stoi(sPort);
        cout << "port       : " << this->_port << endl;
    }

    //sip:alice@atlanta.com
    stringstream ss2;
    ss2 << "sip:";
    ss2 << "[a-z]+@[a-z]+.[a-z][a-z][a-z]";

    regex reg_SipUsernameHost(ss2.str());

    if (regex_match(nameAddress, regex(reg_SipUsernameHost)))
    {
        bFound = true;
        cout << "pattern sip:{username}@{host}  found" << endl;

        string sUserName = nameAddress.substr(nameAddress.find(":") + 1, nameAddress.length());
        sUserName = sUserName.substr(0, sUserName.find("@"));
        this->_userName = sUserName;
        cout << "username : " << sUserName << endl;
        string sHost = nameAddress.substr(nameAddress.find("@") + 1, nameAddress.length());
        this->_host = sHost;
        cout << "host     : " << sHost << endl;
    }

    //"Alice" <sip:alice@atlanta.com>
    stringstream ss3;

    ss3 << "\"([A-Za-z]+)\"(\\s)+<";
    ss3 << "sip:";
    ss3 << "[a-z]+@[a-z]+.[a-z][a-z][a-z]>";

    regex reg_DisplaynamesipUsernameHost(ss3.str());
    if (regex_match(nameAddress, regex(reg_DisplaynamesipUsernameHost)))
    {
        bFound = true;
        cout << "pattern \"DisplayName\" <sip:{username}@{host}>  found" << endl;

        string sDisplayName = nameAddress.substr(0, nameAddress.find("<"));
        sDisplayName.erase(remove(sDisplayName.begin(), sDisplayName.end(), '\"'), sDisplayName.end());
        sDisplayName.erase(remove(sDisplayName.begin(), sDisplayName.end(), ' '), sDisplayName.end());
        this->_displayName = sDisplayName;
        cout << "display name : [" << sDisplayName << "]" << endl;

        string sUserName = nameAddress.substr(nameAddress.find(":") + 1, nameAddress.length());
        sUserName = sUserName.substr(0, sUserName.find("@"));
        this->_userName = sUserName;
        cout << "user name : [" << sUserName << "] " << endl;

        string sHost = nameAddress.substr(nameAddress.find("@") + 1, nameAddress.length());
        sHost = sHost.substr(0, sHost.find(">"));
        this->_host = sHost;
        cout << "host   : [" << sHost << "] " << endl;
    }

    //sip:alice@atlanta.com;tag=asDfgjKl
    stringstream ss4;
    ss4 << "sip:";
    ss4 << "[a-z]+@[a-z]+.[a-z][a-z][a-z]";
    ss4 << ";tag=[A-Za-z]+";

    regex reg_SipUsernameHostTags(ss4.str());
    if (regex_match(nameAddress, regex(reg_SipUsernameHostTags)))
    {
        bFound = true;
        cout << "pattern sip:{username}@{host};tag={tag} found" << endl;

        string delimiter1 = ":";
        string delimiter2 = "@";

        string sipUserName = nameAddress.substr(0, nameAddress.find(delimiter2));
        string userName = sipUserName.substr(sipUserName.find(delimiter1) + 1, sipUserName.length());
        string host = nameAddress.substr(nameAddress.find(delimiter2) + 1, nameAddress.length());
        string tag = nameAddress.substr(nameAddress.find(";") + 1, nameAddress.length());
        host = host.substr(0, host.find(";"));
        this->_userName = userName;
        cout << "username : " << userName << endl;
        this->_host = host;
        cout << "host     : " << host << endl;
        string key = tag.substr(0, tag.find("="));
        string value = tag.substr(tag.find("=") + 1, tag.length());
        this->_parameters.insert(pair<string, string>(key, value));
        cout << "tag      : " << tag << endl;
    }

    //Alice <sip:alice@atlanta.com>;tag=asDfgjKl
    stringstream ss5;
    ss5 << "([A-Za-z]+)(\\s)+<";
    ss5 << "sip:";
    ss5 << "[a-z]+@[a-z]+.[a-z][a-z][a-z]>";
    ss5 << ";tag=[A-Za-z]+";

    regex reg_DisplaynamesipUsernameHostTags(ss5.str());
    if (regex_match(nameAddress, regex(reg_DisplaynamesipUsernameHostTags)))
    {
        bFound = true;
        cout << "pattern DisplayName <sip:{username}@{host}>;tag={tag}  found" << endl;

        string sDisplayName = nameAddress.substr(0, nameAddress.find("<"));
        sDisplayName.erase(remove(sDisplayName.begin(), sDisplayName.end(), ' '), sDisplayName.end());
        this->_displayName = sDisplayName;
        cout << "display name : [" << sDisplayName << "]" << endl;

        string sUserName = nameAddress.substr(nameAddress.find(":") + 1, nameAddress.length());
        sUserName = sUserName.substr(0, sUserName.find("@"));
        this->_userName = sUserName;
        cout << "user name : [" << sUserName << "] " << endl;

        string sHost = nameAddress.substr(nameAddress.find("@") + 1, nameAddress.length());
        sHost = sHost.substr(0, sHost.find(">"));
        this->_host = sHost;
        cout << "host   : [" << sHost << "] " << endl;

        string tag = nameAddress.substr(nameAddress.find(";") + 1, nameAddress.length());
        string key = tag.substr(0, tag.find("="));
        string value = tag.substr(tag.find("=") + 1, tag.length());
        this->_parameters.insert(pair<string, string>(key, value));
        cout << "tag      : " << tag << endl;
    }

    //<sip:alice@atlanta.com?replaces=calid-001;to-tag=totag-001;from-tag=fromtag-001>;myheader=123
    stringstream ss6;
    ss6 << "<sip:";
    ss6 << "[a-z]+@[a-z]+.[a-z][a-z][a-z]\\?";
    ss6 << "((replaces=[0-9A-Za-z\\-\\;]+)|(to-tag=[0-9A-Za-z\\-\\;]+)|(from-tag=[0-9A-Za-z\\-\\;]+))+>";
    ss6 << ";myheader=[0-9]+";

    regex reg_DisplaynamesipUsernameHostTags1(ss6.str());
    if (regex_match(nameAddress, regex(reg_DisplaynamesipUsernameHostTags1)))
    {
        bFound = true;
        cout << "pattern <sip:{username}@{host}>?replaces={tag}|to-tag={tag}|from-tag={tag}>  found" << endl;

        string sFirstPart = nameAddress.substr(0, nameAddress.find("?"));
        string sUserName = sFirstPart.substr(sFirstPart.find(":") + 1, sFirstPart.length());
        sUserName = sUserName.substr(0, sUserName.find("@"));
        this->_userName = sUserName;
        cout << "user name : [" << sUserName << "] " << endl;

        string sHost = sFirstPart.substr(sFirstPart.find("@") + 1, sFirstPart.length());
        this->_host = sHost;
        cout << "host   : [" << sHost << "] " << endl;

        string sTags = nameAddress.substr(nameAddress.find("?") + 1, nameAddress.length());
        sTags = sTags.substr(0, sTags.find(">"));
        cout << "tags : " << sTags << endl;
    }

    if (!bFound)
    {
        //cout << "******** PATTERN NOT FOUND ************" << endl;
    }

    return bFound;
}