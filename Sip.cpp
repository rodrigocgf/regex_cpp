#include "NameAddress.h"
#include <gtest/gtest.h>
#include <iostream>
#include <memory>
using namespace std;

void WaitExit(shared_ptr<NameAddress> nameAddrPrt);


int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    cout << "----------------------------------------------------------------\r\n";

    shared_ptr<NameAddress> nameAddrPrt(new NameAddress());

    WaitExit(nameAddrPrt);

    cout << "----------------------------------------------------------------\r\n";
}

void WaitExit(shared_ptr<NameAddress> nameAddrPrt)
{
    string input;
    string strToParse;

    do
    {
        cout << endl;
        cout << "\t\tType 'quit' to quit." << endl;
        cout << "\t\tType 'parse' to parse a string" << endl;
        cout << "\t\tType 'tests' to run GTEST unit tests" << endl;

        getline(cin, input);
        if (input == "parse")
        {
            cout << "\t\tType the string to be parsed : " << endl;

            getline(cin, strToParse);

            nameAddrPrt->parse(strToParse);
        }
        else if ( input == "tests")
        {
            RUN_ALL_TESTS();
        }
    } while (input.compare("quit") != 0);
}

TEST(SipAddress_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:192.168.10.1";
    cout << endl;
    cout << endl;
    cout << "\tPattern sip:{ip address} ==> [" << pattern << "]" << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(SipAddress_2, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:256.168.10.1";
    cout << endl;
    cout << "\tPattern sip:{ip address} ==> [" << pattern << "]" << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipAddress_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:256.168.10.0";
    cout << endl;
    cout << "\tPattern sip:{ip address} ==> [" << pattern << "]" << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipAddressPort_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:192.168.10.15:5060";
    cout << endl;
    cout << "\tPattern sip:{ip address}:{port} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(SipAddressPort_2, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:257.168.10.15:5060";
    cout << endl;
    cout << "\tPattern sip:{ip address}:{port} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipAddressPort_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:192.168.10.15: 10";
    cout << endl;
    cout << "\tPattern sip:{ip address}:{port} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipAddressPort_4, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip :192.168.10.15:10";
    cout << endl;
    cout << "\tPattern sip:{ip address}:{port} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipUserNameHost_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:alice@atlanta.com";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host} ==> [" <<  pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(SipUserNameHost_2, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip : alice@atlanta.com";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipUserNameHost_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:alice@atlanta.";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHost_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "\"Alice\" <sip:alice@atlanta.com>";
    cout << endl;
    cout << "\tPattern \"DisplayName\" <sip:{userName}@{host}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(DisplaynamesipUsernameHost_2, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "\"Peter\"     <sip:peter@telnyx.com>";
    cout << endl;
    cout << "\tPattern \"DisplayName\" <sip:{userName}@{host}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(DisplaynamesipUsernameHost_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "\"Peter\"     <sip:peter@telnyx.com >";
    cout << endl;
    cout << "\tPattern \"DisplayName\" <sip:{userName}@{host}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHost_4, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "\"Peter\"     <sip:peter@telnyx.c>";
    cout << endl;
    cout << "\tPattern \"DisplayName\" <sip:{userName}@{host}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHost_5, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "\"Peter\"     <sip:peter@ telnyx.com>";
    cout << endl;
    cout << "\tPattern \"DisplayName\" <sip:{userName}@{host}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipUsernameHostTags_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:alice@atlanta.com;tag=asDfgjKl";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(SipUsernameHostTags_2, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:alice@atlanta.com;tag=";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipUsernameHostTags_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip:alice @atlanta.com;tag=Idfaf";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(SipUsernameHostTags_4, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "sip: alice@atlanta.com;tag=Idfaf";
    cout << endl;
    cout << "\tPattern sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTags_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "Alice <sip:alice@atlanta.com>;tag=asDfgjKl";
    cout << endl;
    cout << "\tPattern {DisplayName} sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(DisplaynamesipUsernameHostTags_2, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "Alice <sip:alice@atlanta.com> ; tag=asDfgjKl";
    cout << endl;
    cout << "\tPattern {DisplayName} sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTags_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "Alice <sip:alice@atlanta.com>;tag=";
    cout << endl;
    cout << "\tPattern {DisplayName} sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTags_4, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "Alice sip:alice@atlanta.com>;tag=Idfadf";
    cout << endl;
    cout << "\tPattern {DisplayName} sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTags_5, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "Alice <sip:alice@atlanta.com;tag=Idfadf";
    cout << endl;
    cout << "\tPattern {DisplayName} sip:{userName}@{host};tag={tag}> ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTagsHeader_1, EQ_TRUE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "<sip:alice@atlanta.com?replaces=calid-001;to-tag=totag-001;from-tag=fromtag-001>;myheader=123";
    cout << endl;
    cout << "\tPattern <sip:{userName}@{host}>?replaces={tag};to-tag={tag};from-tag={tag}>;myheader={header} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), true);
}

TEST(DisplaynamesipUsernameHostTagsHeader_2, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "<sip:alice@atlanta.com?replaces=calid-001;to-tag=totag-001;from-tag=fromtag-001>;myheader=";
    cout << endl;
    cout << "\tPattern <sip:{userName}@{host}>?replaces={tag};to-tag={tag};from-tag={tag}>;myheader={header} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTagsHeader_3, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "<sip:alice@atlanta.com?replaces=calid-001;to-tag=totag-001;from-tag=fromtag-001;myheader=header";
    cout << endl;
    cout << "\tPattern <sip:{userName}@{host}>?replaces={tag};to-tag={tag};from-tag={tag}>;myheader={header} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}

TEST(DisplaynamesipUsernameHostTagsHeader_4, EQ_FALSE)
{
    unique_ptr<NameAddress> m_NameAddressPtr;
    m_NameAddressPtr = make_unique<NameAddress>();
    string pattern = "<sip:alice@atlanta.com*replaces=calid-001;to-tag=totag-001;from-tag=fromtag-001>;myheader=header";
    cout << endl;
    cout << "\tPattern <sip:{userName}@{host}>?replaces={tag};to-tag={tag};from-tag={tag}>;myheader={header} ==> [" << pattern << "] " << endl;
    cout << endl;
    EXPECT_EQ(m_NameAddressPtr->parse(pattern), false);
}